# Linear.css
Linear.css is a CSS framework aiming to provide a minimal, but good-looking basis on which a mobile website may be developed.

Linear.css is based on the concept of horizontal blocks or "segments" in which content is placed. There are eight types of segments so far: segment-head, segment-nav, segment-mini, segment-image, segment-text, segment-grid-double, segment-grid-triple, segment-grid-triple-bigleft, segment-grid-triple-bigright.

A test.html file is also available showcasing the capabilities of Linear.
## Principles:
<ul>
<li>Style with classes - most styling should be done by selecting CSS classes.</li>
<li>Open source - released to the public domain so that others can use the content.</li>
<li>Beautiful by default - defaults to a pretty good theme, changing the look is as simple as loading another CSS file with appropriate classes.</li>
<li>Optional - if you load linear.css, you can safely ignore it. It won't style your page until you change the class of your <code>body</code> tag to <code>linear</code>.</li>
</ul>

## Caveats:
Linear can be used to make a sidebar, but we don't recommend it because it works poorly on vertical screens.

Linear's grid (by default) only supports halves and thirds; beyond that you are on your own.

Lastly, use caution with buttons and preformatted areas in Linear; they have known issues.

## Browsers:
Linear has been tested on the following browsers and all essential features work:

* Chrome, Chromium  and derivatives, Opera, Vivaldi (Blink)
* Firefox and derivatives (Gecko)
* Safari, most Linux apps, all iOS browsers (WebKit)
* Microsoft Edge and Windows apps (EdgeHTML)
* Palemoon and Basilisk (Goanna)

Issues have been discovered on the following browsers, both of which are out of date anyway:

* Windows Internet Explorer and old Windows apps (Trident) - Grids do not work.
* Konqueror and some KDE apps (KHTML) - segment-image shows a black background, text in segment-image rises to the top, segment-image is improperly spaced, grids do not work.

The following browsers have not been tested yet. Servo is new, but experimental, and the others are out of date:

* Old Opera (Presto)
* Microsoft Entourage and Mac Internet Explorer (Tasman)
* Mozilla-Samsung research project (Servo)

## Using Linear

Add Linear to your website by downloading linear.css and adding:

        <link rel=stylesheet type=text/css href=/path/to/linear.css>

Adding these fonts is recommended if you intend to use the default theme:

        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Noto+Sans" rel="stylesheet">

Enable Linear by setting the class of `<body>` to `linear`.

Start by adding a header and nav like:

        <div class="segment-head flair-title">Linear 
        <div class=special-button onclick="if (document.getElementById('headnav').style.display == 'none'){document.getElementById('headnav').style.display = 'block';}else{document.getElementById('headnav').style.display = 'none';}">≡</div>
        <div class="segment-nav flair-subtitle" id=headnav><ul><li><a href=#>test</a></li></ul></div></div>

Now add a text area:

        <div class="segment-text flair-text">TEXT GOES HERE</div>
        
...and you're all set.

You might also want to add a hero image. Segment-image requires that you write the background-image property yourself and put the content in a span. I'm working on simplifying it; until then::

        <div class="segment-image flair-black" style="background-image:linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),url('Downloads/animal-339400_1920.jpg');"><span><h1>Cat</h1><p>Say meow!</p></span></div>

## Classes
Linear includes the following positioning classes. All of them should be used on `<div>` elements:

* segment-head: A header.
* segment-nav: A visible, multi-line navbar. Use a list within a div to style it.
* segment-text: A textbox.
* segment-mini: A textbox with less top padding.
* segment-image: An image stretching across page width. Set the background-image property using a style= parameter. To overlay multiple lines of text, place them in a `<span>` tag.

I recommend adding your own CSS classes for styles. By default, the following theming classes are included:

* flair-text: black-on-white.
* flair-warning: white-on-red
* flair-info: white-on-blue
* flair-success: white-on-green
* flair-black: white-on-black, recommended for images
* flair-title: white-on-black, Comfortaa/Montserrat font, recommended alternative for images
* flair-nav: white-on-black, all uppercase, li:hover highlights

Additionally, the following special classes are available:

* special-button: A div that looks like and is shaped like a button. Recommend not using; is very buggy.
* mod-center: Centers text.
* mod-24em: Use with segment-image. Reduces image height to 24em